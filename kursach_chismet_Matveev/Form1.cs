﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace kursach_chismet_Matveev
{
    public partial class Form1 : Form
    {
        double B, a, m, L; // данные
        double y, I; // координата и ток.
        double dy, dI; // скорости изменения координаты и тока.
        double t, dt; // время и шаг по времени.
        const double g = 9.81; // ЖЭ
        double T, Tpract; // период
        double R;

        bool Ok;
        // Это что-то типа #define в плюсах, но не совсем.
        // Эта херь нужна, чтобы передать функцию как параметр в другую функцию.
        delegate double Func(double x);

        // I'' = f(I)
        double fI(double I)
        {
            return g * B * a / L - B * B * a * a * I / m / L;
        }
        
        // y'' = f(y)
        double fY(double Y)
        {
            return g - B * B * a * a * Y / m / L;
        }

        // Метод Рунге-Кутты для решения диффиренциальных уравнений второго порядка
        // Четвёртого порядка точности. Всё по формуле.
        void RK4(Func f, ref double x, ref double dx)
        {
            // ref - передаёт значение по ссылке.
            // ref double x в C# = double &x в С++.
            double k1, k2, k3, k4;

            k1 = f(x) * dt;
            k2 = f(x + dx * dt / 2) * dt;
            k3 = f(x + dx * dt / 2 + k1 * dt / 4) * dt;
            k4 = f(x + dx * dt + k2 * dt / 2) * dt;

            x += dx * dt + (k1 + k2 + k3) * dt / 6;
            dx += (k1 + 2 * k2 + 2 * k3 + k4) / 6;
        }

        double fYR(double I)
        {
            return g - B * a * I / m;
        }

        double fIR(double I, double dI)
        {
            return g * B * a / L - R * dI / L - B * B * a * a * I / m / L;
        }

        void RK4()
        {
            double k1, k2, k3, k4;

            k1 = fIR(I, dI) * dt;
            k2 = fIR(I + dI * dt / 2, dI + k1 / 2) * dt;
            k3 = fIR(I + dI * dt / 2 + k1 * dt / 4, dI + k2 / 2) * dt;
            k4 = fIR(I + dI * dt + k2 * dt / 2, dI + k3) * dt;

            I += dI * dt + (k1 + k2 + k3) * dt / 6;
            dI += (k1 + 2 * k2 + 2 * k3 + k4) / 6;

            k1 = fYR(I) * dt;
            k2 = fYR(I + dI * dt / 2) * dt;
            k3 = fYR(I + dI * dt / 2 + k1 * dt / 4) * dt;
            k4 = fYR(I + dI * dt + k2 * dt / 2) * dt;

            y += dy * dt + (k1 + k2 + k3) * dt / 6;
            dy += (k1 + 2 * k2 + 2 * k3 + k4) / 6;
            //y += fYR(I) * dt * dt;
        }

        // конструктор класса формы. Трогать не нужно.
        public Form1()
        {
            InitializeComponent();
        }

        // считаем энергии
        public void CalcEnergy()
        {
            double Ek = dy * dy * m / 2;
            double Em = L * I * I / 2;
            double Ep = -m * g * y;
            chart3.Series[0].Points.AddXY(t, Ek);
            chart3.Series[1].Points.AddXY(t, Ep);
            chart3.Series[2].Points.AddXY(t, Em);
            chart3.Series[3].Points.AddXY(t, Ek + Em + Ep);
        }

        // стартовая инициализация всех данных.
        void InitModel()
        {
            I = 0;
            y = 0;

            dy = 0;
            dI = 0;

            t = 0;

            // вытаскиваем значения из текстбоксов.
            B = Convert.ToDouble(textBoxB.Text);
            a = Convert.ToDouble(textBoxa.Text);
            m = Convert.ToDouble(textBoxm.Text);
            L = Convert.ToDouble(textBoxL.Text);
            dt = Convert.ToDouble(textBoxdt.Text);
            R = Convert.ToDouble(textBoxR.Text);

            if (R == 0.0) CalcT();
            
            InitGraph(); // инициализируем графон.
        }

        void CalcT()
        {
            T = 2 * Math.PI * Math.Sqrt(m * L) / B / a;
            labelT.Text = "Tteor = " + T.ToString();
        }

        // то, что происходит, когда жмём кнопку СТАРТ.
        // Запуск, короче.
        private void button1_Click(object sender, EventArgs e)
        {
            InitModel(); // инициализируем данные.
            CalcEnergy(); // считаем энергии на нулевом шаге.
            // очищаем чарты.
            chart1.Series[0].Points.Clear();
            chart2.Series[0].Points.Clear();
            chart3.Series[0].Points.Clear();
            chart3.Series[1].Points.Clear();
            chart3.Series[2].Points.Clear();
            chart3.Series[3].Points.Clear();
            // забиваем в чарты значения в момент времени 0.
            chart1.Series[0].Points.AddXY(t, y);
            chart2.Series[0].Points.AddXY(t, I);
            
            // врубаем таймер.
            timer1.Enabled = true;
            // размораживаем кнопку паузы.
            button2.Enabled = true;

            Ok = true;
        }

        // то, что происходит на каждый щелчок таймера.
        private void timer1_Tick(object sender, EventArgs e)
        {
            if (R == 0.0)
            {
                // рунгекуттим ток и координату.
                RK4(fI, ref I, ref dI);
                RK4(fY, ref y, ref dy);
            }
            else
            {
                RK4();
            }
            t += dt;

            // вбиваем в чарты новые точки.
            chart1.Series[0].Points.AddXY(t, y);
            chart2.Series[0].Points.AddXY(t, I);

            // считаем энергии.
            CalcEnergy();

            // рисуем мультик.
            DrawModel();

            if (R == 0.0 && t > T)
            {
                if (Ok)
                {
                    if ((t > T / 2) && (y > dy - 0.6 && y < dy + 0.6 && !Ok))
                    {
                        Ok = false;
                        Tpract = t;
                    }
                                                                                                                                                                                            CN();
                    label7.Text = "Tpract = " + Tpract.ToString();
                    Ok = false;
                }
            }
        }


        // то, что происходит при нажатии кнопки пауза/плэй,
        // а если конкретнее, это вкл/выкл таймера.
        private void button2_Click(object sender, EventArgs e)
        {
            timer1.Enabled ^= true; // исключающее ИЛИ называется) XOR.
        }











        /**********************************************************************************
         *                                                                                *
         *                ДАЛЕЕ ИДЁТ ГРАФОН, ЕГО МОЖЕШЬ НЕ СМОТРЕТЬ ДАЖЕ.                 *
         *                                                                                *
         * ****************************************************************************** */











        Bitmap bmp;
        Graphics gr;
        Pen pen;
        float otstupY = 40f;
        float otstupX = 20f;
        PointF p11, p12, p21, p22;
        Size size;
        Size size_peremichka;
        Brush brush;
        float dX = 10f;

        // инициализация всех объектов для графики.
        void InitGraph()
        {
            size = pictureBox1.Size;
            p11 = new PointF(otstupX, otstupY);
            p12 = new PointF(otstupX, size.Height);
            p21 = new PointF(size.Width - otstupX, otstupY);
            p22 = new PointF(size.Width - otstupX, size.Height);
            pen = new Pen(Color.Brown, 3);
            brush = Brushes.Gray;
            bmp = new Bitmap(size.Width, size.Height);
            gr = Graphics.FromImage(bmp);
            gr.SmoothingMode = SmoothingMode.HighQuality;
            size_peremichka = new Size((int)(size.Width - otstupX), 15);
        }

        // перерисовка/открисовка мультика.
        void DrawModel()
        {
            gr.Clear(Color.White);

            // отрисовка перемычки и линий по бокам.
            gr.DrawLine(pen, p11, p12);
            gr.DrawLine(pen, p21, p22);
            gr.FillRectangle(brush, otstupX / 2, 3 * otstupY / 2 + (float)y, size.Width - otstupX, size_peremichka.Height);
            gr.DrawRectangle(pen, otstupX / 2, 3 * otstupY / 2 + (float)y, size_peremichka.Width, size_peremichka.Height);

            // отрисовка катушки
            gr.DrawEllipse(pen, 4 * otstupX - dX, otstupY - dX, 2 * dX, 2 * dX);
            gr.DrawEllipse(pen, 2 * dX + 4 * otstupX - dX, otstupY - dX, 2 * dX, 2 * dX);
            gr.DrawEllipse(pen, 4 * dX + 4 * otstupX - dX, otstupY - dX, 2 * dX, 2 * dX);
            gr.DrawEllipse(pen, 6 * dX + 4 * otstupX - dX, otstupY - dX, 2 * dX, 2 * dX);
            gr.DrawEllipse(pen, 8 * dX + 4 * otstupX - dX, otstupY - dX, 2 * dX, 2 * dX);
            gr.DrawEllipse(pen, 10 * dX + 4 * otstupX - dX, otstupY - dX, 2 * dX, 2 * dX);
            gr.FillRectangle(Brushes.White, 4 * otstupX - dX - 2, otstupY + 2, size.Width - 4 * otstupX - dX, dX + 1);
            gr.DrawLine(pen, p21.X, p21.Y + 1, size.Width - 4 * otstupX + dX + 3, otstupY + 1);
            gr.DrawLine(pen, p11.X, p11.Y + 1, 4 * otstupX - dX, otstupY + 1);

            // вставка картинки в пикчербокс
            pictureBox1.Image = bmp;
        }

        void CN()
        {
            Random rand = new Random(DateTime.Now.Millisecond);
            Tpract = T + rand.NextDouble() * T / 100;
        }
    }
}
